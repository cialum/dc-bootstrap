#!/bin/sh

if [ -n "$GITLAB_INSTANCE_URL_PATH" ] && [ -n "$RUNNER_AUTHENTICATION_TOKEN_PATH" ]; then
    export GITLAB_INSTANCE_URL=$(cat $GITLAB_INSTANCE_URL_PATH)
    export RUNNER_AUTHENTICATION_TOKEN=$(cat $RUNNER_AUTHENTICATION_TOKEN_PATH)
fi

# Register and launch gitlab-runner
#
# https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1846
# Supported standard values are: bridge, host, none, and container:<name|id>.
# Any other value is taken as a custom network's name to which this container should connect to.
#
# https://docs.gitlab.com/runner/configuration/advanced-configuration.html#example-2-mount-a-host-directory-as-a-data-volume
# Mount runner volumes to service volume mounts
if [ -n "$RUNNER_AUTHENTICATION_TOKEN" ] && [ -n "$GITLAB_INSTANCE_URL" ]; then
  gitlab-runner register \
    --non-interactive \
    --url "$GITLAB_INSTANCE_URL" \
    --token "$RUNNER_AUTHENTICATION_TOKEN" \
    --description "project_runner" \
    --executor "docker" \
    --docker-image "$RUNNER_IMAGE"\
    --docker-helper-image "$HELPER_IMAGE"\
    --docker-network-mode runner_network
fi

exec gitlab-runner "$@"
